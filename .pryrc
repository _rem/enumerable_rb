# -*- ruby -*-

lib = Dir['./lib/**/*.rb'].each(&method(:require))

Pry.config.requires = lib
Pry.config.prompt_name = "enumerable_rb_dev"
Pry.config.editor = proc { |file, line| "emacsclient +#{line} #{file}" }
Pry.config.auto_indent = false # Comment out when running repl outside emacs' shell
