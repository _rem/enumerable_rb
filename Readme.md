# enumerable_rb

A simple kata for reviewing `Enumerable` and blocks.

Tests review how to use some of `Enumerable`'s methods. The solution reviews
how to use blocks to implement parts of `Enumerable`.

The tests only check for a method's "happy path"; no edge cases, no errors,
no `Enumerator`. All tests pass with Ruby's `Enumerable`.

Enumerable methods included:

```
count
find          (detect)
inject        (reduce)
each_with_object
find_all      (select)
map           (collect)
group_by
take
drop
each_with_index
first
all?
none?
any?
min_by
max_by
include
reject
```

The methods in parenthesis aren't actually included but since they are aliases
their implementation is considered the same.

## Setup Enumerable_rb

In order to get this kata up and running you need to follow
these steps in your terminal from whichever folder will contain the `enumerable_rb` folder.

```
git clone https://notabug.org/rem/enumerable_rb.git ./enumerable_rb
cd enumerable_rb
```

Install dependencies locally.

```
bundle install --path .bundle/gems
```

If you run into problems chances are you need to sudo-gem-install a few
gems. Check `Gemfile` for details.

## Export terminal session variables

Export your project's environment, as well as the `bin` folder to PATH, whenever
we start a new terminal session.
The environment variable allows us to run successfully commands such as `autotest`.
Temporarily appending the local `bin` directory to the PATH allows us to run
`rake <task>` rather than `bundle exec rake <task>`.

```
export ENUMERABLE_RB_ENV="development"
export PATH="$PWD/bin:$PATH"
hash -r 2>/dev/null || true
```
Alternatively, create a [simple shell function](https://notabug.org/rem/.dots/src/master/zsh/functions/hacktime.sh) to do it for you.

To list all available rake tasks simply run `rake -T`.

Most binstubs execute a command rather than start a session. Hence most are run from
tasks. Those not included in a task are usually run on their own terminal session.

`autotest` Run all tests then only those related to the code you are working on.
`repl` Start a REPL session loading all files in `lib`. Restart REPL session after
making any changes to `lib`'s contents.

If you want to step through some of the code during development use from anywhere:

```
require 'pry-byebug'; binding.pry
```
