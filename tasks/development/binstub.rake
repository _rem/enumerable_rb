# -*- coding: utf-8 -*-

desc "Create a binstub for a Gem: rake 'binstub[gem_name_as_in_Gemfile]'"
task :binstub, [:name] do |_, args|
  unless (name = args[:name])
    $stderr.puts "ERROR: Must provide argument: rake 'binstub[gem_name]'"
    exit(1)
  end

  sh "bundle binstubs #{name}"
end
