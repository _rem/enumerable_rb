# -*- coding: utf-8 -*-

namespace :debride do
  desc "Analize lib folder code with debride"
  task :lib do
    sh "debride lib"
  end
end
