# -*- coding: utf-8 -*-

namespace :flay do
  desc "Analize lib folder code with flay"
  task :lib do
    sh "flay -d lib"
  end
end
