# -*- coding: utf-8 -*-

namespace :flog do
  desc "Analize lib folder code with flog"
  task :lib do
    sh "flog -ga lib"
  end
end
