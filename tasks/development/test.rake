# -*- coding: utf-8 -*-
require 'rake/testtask'

Rake::TestTask.new do |t|
  t.libs = FileList["lib/**/*.rb"]
  t.test_files = FileList["test/**/test*.rb"]
  t.warning = true
end
