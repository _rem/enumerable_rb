# -*- coding: utf-8 -*-
require_relative "./../_helper"
require_relative "./../../lib/my_enumerable"

class HashStandIn
  include MyEnumerable

  attr_reader :dummy_data

  def initialize **keywords
    @dummy_data = { **keywords }
  end

  def each &block
    # dummy_data.each(&block)

    # or:
    data = dummy_data.to_a

    for pair in 0...(data.length)
      block.call data[pair]
    end
  end
end

class ArrayStandIn
  include MyEnumerable

  attr_reader :items

  def initialize *items
    @items = *items.flatten
  end

  def each &block
    # items.each(&block)

    # or:
    for item in 0...(items.length)
      block.call items[item]
    end
  end
end
